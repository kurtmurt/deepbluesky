<!DOCTYPE html>
    <head>
    <title>Happy Birthday App</title>
    <!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
    <style>
        html,body { 
            height: 100%; 
            margin: 0px; 
            padding: 0px; 
        }
        #full { 
            background: #797979;
            height: 100%;
            text-align: center;
        }
        .page-wrapper {
            position: relative;
            width: 100%;
            display: block;
        }
        .form-container {
            position: absolute;
            display: block;
            height: 330px;
            padding: 20px;
            width: auto;
            max-width: 614px;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            margin: auto;
            background: #ffffff;
        }
    </style>
    </head>
    <body>
        <div class="page-wrapper" id="full">
            <div class="form-container">
                <h1>Get an email on your birthday from us!</h1>
                <div id="login">
                    <h2>Users details</h2>
                    <form action="" method="post">
                        <label>Firstname :</label>
                        <input type="text" name="firstname" id="firstname" required="required" placeholder="Please Enter Your Firstname"/><br /><br />
                        <label>Surname :</label>
                        <input type="text" name="surname" id="surname" required="required" placeholder="Please Enter Your Surname"/><br/><br />
                        <label>Birthday :</label>
                        <input type="date" name="birthday" id="birthday" required="required" placeholder="Please Enter Your DOB"/><br/><br />
                        <label>Email :</label>
                        <input type="email" name="email" id="email" required="required" placeholder="Please Enter Your email"/><br/><br />
                        <input type="submit" value=" Submit " name="submit"/><br />
                    </form>
                    <?php
                        if(isset($_POST["submit"])){
                            $hostname='127.0.0.1';
                            $username='root';
                            $password='';
                        try {
                            $dbh = new PDO("mysql:host=$hostname;dbname=dbs_test",$username,$password);

                        $firstname = $_POST['firstname'];
                        $surname = $_POST['surname'];
                        $birthday = $_POST['birthday'];
                        $email = $_POST['email'];

                        //Verifcation 
                        if (empty($firstname) || empty($surname) || empty($birthday) || empty($email)) {
                            $error = "Complete all fields";
                        }

                        // Email validation

                        if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                            $error = "Enter a  valid email";
                        }

                        if(!isset($error)){
                        //no error

                        $sthandler = $dbh->prepare("SELECT email FROM user WHERE email = :email");
                        $sthandler->bindParam(':email', $email);
                        $sthandler->execute();

                        if($sthandler->rowCount() > 0) {
                            echo "$email exists! cannot insert";
                            exit();
                        } else {
                            //Securly insert into database
                            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // <== add this line
                            $sql = "INSERT INTO user (firstname, surname, birthday, email)
                            VALUES ('".$_POST["firstname"]."','".$_POST["surname"]."','".$_POST["birthday"]."','".$_POST["email"]."')";
                            }
                        }
                        if ($dbh->query($sql)) {
                            echo "<script type='text/javascript'>alert('New Record Inserted Successfully - You\'ll get your birthday message soon/on your birthday!');</script>";
                        }
                        else {
                            echo "<script type='text/javascript'>alert('Data not successfully Inserted.');</script>";
                        }

                        $dbh = null;
                    }
                        catch(PDOException $e)
                    {
                        echo $e->getMessage();
                    }}

                    ?>
                </div>
            </div>
        </div>
    </body>
</html>